# Time Parser Readme

Time Parser is an application created as a part of interview process for Quality Engineer for Alteryx.  

Application parses times in format H:M:S with 12 or 24 hour from input provided as a string or file and prints found 
valid times found in the input.  

Valid times are: text 13:30:11 text, text12:0:0am text, 11:22:33 PM  
Invalid times are: text 13:30:11 am, text12:0:0amtext, 25:00:00, text15:00pm 

## Usage

To run the application, run `python time-parser.py <text-or-file-path>`

Examples:
```
> python time-parser.py '0:0:0'
Parsing times from string: "0:0:0"
Times parsed from the input are: 00:00:00

> python time-parser.py 'text11:35:3text'
Parsing times from string: "text11:35:3text"
Times parsed from the input are: 11:35:03

> python time-parser.py 'input.txt'
Parsing times from content of file: "input.txt"
Times parsed from the input are: 00:00:00, 10:10:10, 22:10:10, 10:10:10

> python time-parser.py 'non-existent-file.txt'
Parsing times from string: "non-existent-file.txt"
No valid times were found in the provided input. See help for more info.
```

## Help
```
> python time-parser.py -h
usage: time-parser.py [-h] input

Parse times from the given string or file path. Application accepts any times in format H:M:S in 12 or 24 hour
format. Valid examples: text 13:30:11 text, text 12:0:0am text, 11:22:33 PM. Invalid examples: 0:0:0 am, 15:00pm,
11:61:74pmtext, 25:00:00

positional arguments:
  input       path to the file or string to be parsed

optional arguments:
  -h, --help  show this help message and exit
```
