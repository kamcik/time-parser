import os
import re
from datetime import datetime
import argparse

string_to_parse = ''
parsed_times = []

# defining command line arguments parser
parser = argparse.ArgumentParser(description='Parse times from the given string or file path, accepting times '
                                             'in format H:M:S in both 12/24h format and returning in 24 hour format. '
                                             'See readme for more info.')
parser.add_argument('input',
                    action='store',
                    default='',
                    help='path to the file or string to be parsed')

args = parser.parse_args()
script_input = args.input

# Test if the provided string is an valid path, if yes, reading the content; else using it as a string
if os.path.exists(args.input):
    with open(args.input) as f:
        string_to_parse = f.read()
        f.close()
    print('Parsing times from content of file: "{}"'.format(args.input))
else:
    string_to_parse = args.input
    print('Parsing times from string: "{}"'.format(string_to_parse))

# parsing times (from 0:0:0 to 99:99:99PM or 99:99:99 PM) in the 'string_to_parse' using regex
time_pairs = re.findall('(?i)([0-9]{1,2}:[0-9]{1,2}:[0-9]{1,2})\s{0,1}(\S*)', string_to_parse)

# looping trough all times found, converting them into datetime format
for time_value, time_format in time_pairs:
    # ignoring invalid values like 25:61:61 or 0:0:0pm
    try:
        if time_format.strip().upper() == 'AM' or time_format.strip().upper() == 'PM':
            converted_time = datetime.strptime('{} {}'.format(time_value, time_format.strip().upper()), '%I:%M:%S %p')
        else:
            converted_time = datetime.strptime(time_value, '%H:%M:%S')
    except ValueError:
        continue
    parsed_times.append(converted_time.strftime('%H:%M:%S'))

# Printing parsed times or message when no times were found.
if not parsed_times:
    print('No valid times were found in the provided input. See help for more info.')
else:
    print('Times parsed from the input are: ' + ', '.join(parsed_times))
